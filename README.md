# timely
Background: A digital forensic tool created using vis.js network library. It allows users to log their data for cases in a secure local environment in an organized format to reduce the paperwork and to help investigators easily access data in a quick manner.  
  
Features: Displays data in a timeline, table, and allows exporting of data into an excel worksheet.